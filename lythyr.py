#!/usr/bin/python3
# Remailer that serves as a mailing list, implemented as a mailfilter
# for an established mail server such as postfix or exim4.

import gnupg, settings

# Initialize a GPG instance with a custom home directory
# and set up gpg2 as a default binary.
# If the directory does not exist it will be created.
gpg = gnupg.GPG(gnupghome='.lythyr', gpgbinary='/usr/bin/gpg2', verbose=settings.DEBUG, options=['--yes'])

key_settings = gpg.gen_key_input(
    key_type=settings.KEY_TYPE,
    key_length=settings.KEY_LENGTH,
    name_real=settings.REAL_NAME,
    name_comment=settings.KEY_COMMENT,
    name_email=settings.LIST_ADDRESS,
    # TODO: add passphrase from settings (rethink how to)
    # passphrase=settings.PASSPHRASE,
)

# Get list key, create if it doesn't exist
key = gpg.list_keys(secret=True)[0] if gpg.list_keys(secret=True) else gpg.gen_key(key_settings)

print('Keys in the keyring:')
print(gpg.list_keys()[1:])

# Encrypt with all keys in the keyring
recipient_fingerprints = [ key['fingerprint'] for key in gpg.list_keys()[1:] ]
sealed_ascii = gpg.encrypt(str(data), recipient_fingerprints[0], sign=key, always_trust=True)
print("OK: ", sealed_ascii.ok)
print("SEALED ASCII: ", str(sealed_ascii))
